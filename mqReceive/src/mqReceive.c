/*
 * mqReceive.c
 *
 *  Created on: Dec 1, 2019
 *      Author: vagrant
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <mqueue.h>
#include <pthread.h>

#define BUFFER_SIZE 1024

static void *buf;
static size_t buffer_size;

static void                     /* スレッド開始関数 */
thread_function(union sigval sv)
{
    mqd_t mqdes = *((mqd_t *) sv.sival_ptr);

    struct sigevent sev;

    sev.sigev_notify = SIGEV_THREAD;
    sev.sigev_notify_function = thread_function;
    sev.sigev_notify_attributes = NULL;
    sev.sigev_value.sival_ptr = &mqdes;   /* スレッド関数に渡す引き数 */

    if (mq_notify(mqdes, &sev)) {
    	perror("mq_notify");
    	exit(5);
    }

    ssize_t nr;
    if ((nr = mq_receive(mqdes, buf, buffer_size, NULL)) < 0) {
        perror("mq_receive");
        free(buf);
        exit(6);
    }
    if (fputs(buf, stdout) == EOF) {
    	perror("fputs");
    	exit(7);
    }
}

int
main(int argc, char *argv[])
{
	if (argc != 2) {
		fprintf(stderr, "Usage: mqSend <qName>\n");
		exit(1);
	}
    mqd_t mqdes;
    if ((mqdes = mq_open(argv[1], O_RDONLY)) == (mqd_t) -1) {
    	perror("mq_open");
    	exit(2);
    }
    struct mq_attr attr;
    if (mq_getattr(mqdes, &attr)){
        perror("mq_getattr");
        exit(3);
    }
    /* 最大メッセージサイズを決定し、
       メッセージ受信用のバッファーを確保する */
    if ((buf = malloc(attr.mq_msgsize)) == NULL) {
        perror("malloc");
        exit(4);
    }
    buffer_size = attr.mq_msgsize;
    union sigval sv;
    sv.sival_ptr = &mqdes;
    thread_function(sv);
    pause();
    exit(0);
}
