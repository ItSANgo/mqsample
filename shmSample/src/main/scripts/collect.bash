#! /bin/bash -u

output_dir="${SHMSAMPLE_TMPDIR-/tmp}"
list_target_dir="/tmp"

ss -a >"$output_dir/ss-a.txt"
ip addr >"$output_dir/ip_addr.txt"
ls -laR "$list_target_dir" >"$output_dir/ls-laR.txt" 2>"$output_dir/ls-laR.err.txt"
cp /proc/consoles "$output_dir"
