/*
 * shmsample_inval.c
 *
 *  Created on: 2019/12/07
 *      Author: vagrant
 */

#include "shmsample.h"

const struct memory INVAL_MEMORY = {
        0,
        MAP_FAILED
};
