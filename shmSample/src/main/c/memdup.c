/*
 * memdup.c
 *
 *  Created on: 2019/12/06
 *      Author: vagrant
 */
#include "memdup.h"

/* see:
 * https://stackoverflow.com/questions/13663617/memdup-function-in-c
 */
void *memdup(const void* mem, size_t size) {
   void *out = malloc(size);
   if (out != NULL) {
       memcpy(out, mem, size);
   }
   return out;
}
