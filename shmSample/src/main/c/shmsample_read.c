/**
 * 
 * */

#include "shmsample.h"

struct memory shmsample_read(const char *name, size_t size) {
    int descriptor = shm_open(name, O_RDONLY, 0400);
    if (descriptor < 0) {
        return INVAL_MEMORY;
    }
    void *addr = mmap(NULL, size, PROT_READ, MAP_SHARED, MAP_SHARED, descriptor, 0);
    if (addr == MAP_FAILED) {
        close(descriptor);
        return INVAL_MEMORY;
    }
    close(descriptor);
    struct memory ret = { 0, MEM_INVAL };
    ret.addr = memdup(addr, size);
    if (ret.addr == NULL) {
        ret.addr = MEM_INVAL;
        munmap(addr, size);
        return INVAL_MEMORY;
    }
    munmap(addr, size);
    ret.size = size;
    return ret;
}