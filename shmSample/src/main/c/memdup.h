/*
 * memdup.h
 *
 *  Created on: 2019/12/06
 *      Author: vagrant
 */

#ifndef MAIN_OLDC_MEMDUP_H_
#define MAIN_OLDC_MEMDUP_H_

#include <string.h>
#include <stdlib.h>

extern void *memdup(const void* mem, size_t size);

#endif /* MAIN_OLDC_MEMDUP_H_ */
