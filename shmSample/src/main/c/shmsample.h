/*
 * shmSample.h
 *
 *  Created on: 2019/12/07
 *      Author: vagrant
 */

#ifndef MAIN_OLDC_SHMSAMPLE_H_
#define MAIN_OLDC_SHMSAMPLE_H_

#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h> /* mode 定数用 */
#include <fcntl.h> /* O_* 定数の定義用 */ 
#include <sys/mman.h>

#include "memdup.h"

struct memory {
    size_t size;
    void *addr;
};

extern struct memory INVAL_MEMORY;

extern long long shmsample_write(const char *name, mode_t mode, struct memory in_mem);
extern struct memory shmsample_read(const char *name, size_t len);
extern void shmsample_free(struct memory mem);

#endif /* MAIN_OLDC_SHMSAMPLE_H_ */
