/*
 * shmsample_create.c
 *
 *  Created on: 2019/12/07
 *      Author: vagrant
 */
#include "shmsample.h"

long long shmsample_write(const char *name, mode_t mode, struct memory in_mem) {
    int descriptor = shm_open(name, O_RDMR | O_CREATE, mode);
    if (descriptor < 0) {
        return -1;
    }
    void *addr = mmap(NULL, in_mem.size, PROT_WRITE, MAP_SHARED, descriptor, 0);
    if (addr == MAP_FAILED) {
        close(descriptor);
        return -2;
    }
    close(descriptor);
    memcpy(addr, in_mem.addr, in_mem.size);
    if (memunmap(addr, m.size)) {
        return -3;
    }
    struct memory ret;
    ret.size = in_mem.size;
    ret.addr = addr;
    return ret.size;
}
