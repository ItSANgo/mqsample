/*
 * mqSend.c
 *
 *  Created on: Dec 1, 2019
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <mqueue.h>

#define BUFFER_SIZE 1024

int
main(int argc, char *argv[])
{
	if (argc != 3) {
		fprintf(stderr, "Usage: mqSend <qName> <fileName>\n");
		exit(1);
	}
    mqd_t mqdes;
    if ((mqdes = mq_open(argv[1], O_WRONLY | O_CREAT, (mode_t) 0666,
    		(struct mq_attr *) NULL)) == (mqd_t) -1) {
    	perror("mq_open");
    	exit(2);
    }
    FILE *fp;
    if ((fp = fopen(argv[2], "r")) == NULL) {
    	perror("fopen");
    	exit(3);
    }
    char buf[BUFFER_SIZE];
    while (fgets(buf, BUFFER_SIZE, fp)) {
    	int priority = atoi(buf);
    	size_t length = strlen(buf) + 1u;
    	if (mq_send(mqdes, buf, length, priority)) {
    		perror("mq_send");
    		exit(4);
    	}
    }
	exit(0);
}
